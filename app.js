const fs = require('fs');
const path = require('path');
const indeed = require('indeed-scraper');

const baseQuery = {
    host: 'www.indeed.com',
    query: 'Software Engineer',
    city: 'San Fransisco, CA',
    radius: '80',
    level: 'entry_level',
    jobType: 'intern',
    maxAge: '7',
    sort: 'date',
    limit: 500
};

const queries = [
    baseQuery,
    { ...baseQuery, query: 'Electrical Engineer', },
    { ...baseQuery, query: 'Developer', },
    { ...baseQuery, query: 'Embedded Engineer', },
]

queries.forEach(query => {
    indeed.query(query).then(res => {
        let data = JSON.stringify(res).replace(/\:null/gi, "\:\"null\"");
        const outDir = 'jobs'
        if(!fs.existsSync(outDir)){
            fs.mkdirSync(outDir)
        }
        fs.writeFileSync(path.join(outDir, `jobs-${query.query}-${(new Date()).getTime()}.json`), data);
    });
})
